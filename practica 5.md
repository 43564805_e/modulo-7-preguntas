1.- Què és una LAN i quines diferències hi ha amb una WAN?

LAN son las siglas de Local Area Network, red de area local. Una LAN es una red que conecta los ordenadores en un área relativamente pequeña y predeterminada (como una habitación, un edificio, o un conjunto de edificios). 
La diferencia entre LAN y WAN es bàsicamente el tamaño de la red en el que se mueven seindo la LAN para redes pequeñas como unas oficinas, una casa, etc.

2.- Busca quines són les capes del model OSI.

![images](p5.png)





















3.- Quina és la unitat d'informació de les capes OSI: física, enllaç de dades, de
xarxa i de transport.

Físico: Bits 
Enlace de datos: Frames
Red: Packets
Transporte: Segments


4.- Per a què serveix una adreça IP y una màscara de xarxa?

Una IP sirve para localizar a un usuario en la red.
La màscara de red sirve para delimitar el àmbito de una red de ordenadores, su función es indicar a los dispositivos qué parte de la dirección IP  és el nùmero de la res, incluida la subred, y qué parte es el correspondiente host.





5.- Explica les diferents classes d'adreçament IP: A, B, C. Indica per a cada
classe el nombre de host que permet.

IP CLASE A: De 10.0.0.0 a 10.255.255.255, que son utilizadas generalmente para grandes redes privadas, por ejemplo de alguna empresa trasnacional.

IP CLASE B: De 172.16.0.0 a 172.31.255.255, que son usadas para redes medianas, como de alguna empresa local, escuela o universidad.

IP CLASE C:  192.168.0.0 a 192.168.255.255, que son usadas para las redes más pequeñas, como redes domésticas.

6.- Què són les classes D i E? Busca-les i explica-les.

IP CLASE D: La clase D tiene la dirección IP 224.0.0.0 a 239.255.255.255, esta última dirección, es reservada para la multidifusión. Los datos de la multidifusión no están destinados para un host en concreto, por eso no hay necesidad de extraer direcciones de host de la dirección IP, y la clase D no tiene ninguna máscara de subred.

IP CLASE E: Esta clase IP está reservada para fines experimentales. Las direcciones IP de esta clase van de 240.0.0.0 a 255.255.255.254 . Como la clase D, también esta clase no está equipada con máscara de subred.

7.- Explica l’encapsulació dels paquets entre els diferents nivells. Per a què
serveix?


1. Abierto: Hace que el miembro de la clase pueda ser accedido desde el exterior de la Clase y cualquier parte del programa. 
2. Protegido: Solo es accesible desde la Clase y las clases que heredan (a cualquier nivel). 
3. Semi cerrado: Solo es accesible desde la clase heredada. 
4. Cerrado: Solo es accesible desde la Clase.


8.- Quina diferència hi ha entre les adreces públiques i privades. Quines són les
adreces privades?

La pública es el identificador de nuestra red desde el exterior, es decir, la de nuestro router de casa, que es el que es visible desde fuera, mientras que la privada es la que identifica a cada uno de los dispositivos conectados a nuestra red, por lo tanto, cada una de las direcciones IP que el router asigna a nuestro ordenador, móvil, tablet o cualquier otro dispositivo que se esté conectado a él.


9.- Què són les adreces unicast, broadcast i multicast? Quines diferències hi ha
entre elles?

Unicast : La unidifusión o difusión única (en inglés: unicast) es el envío de información desde un único emisor a un único receptor.


Broadcast: es una forma de transmisión de información donde un nodo emisor envía información a una multitud de nodos receptores de manera simultánea, sin necesidad de reproducir la misma transmisión nodo por nodo.

Multicast: es un método de envío simultáneo de paquetes (a nivel de IP) que tan sólo serán recibidos por un determinado grupo de receptores, que están interesados en los mismos

10.- ¿Quines topologies de xarxa coneixes?


En estrella (star) 
En anillo (ring) o circular 
En malla (mesh) 
En árbol (tree) o jerárquica

11.- Què és ARPANET?


Fue creada a cargo del departamento de defensa de los Estados Unidos como medio de comunicación por los diferentes organismos del país. El primer nodo se creó en la universidad de California, y fue el eje central de Internet hasta el 1990 depues d eterminar la transición del protocolo TCP/IP EN 1983.